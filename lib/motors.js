// https://www.npmjs.com/package/motor-hat

// get a motor-hat instance with the following initialized:
// * I2C address for the motor hat
// * a dc motor on ports 'M3' and 'M4'
let spec = {
  address: 0x60,
  dcs: ['M3', 'M4']
};
var motorHat = require('motor-hat')(spec);
motorHat.init();

const MOTOR_WIRING_DIRECTION = -1;

// stop motors on process termination

process.stdin.resume(); //so the program will not close instantly

function exitHandler(options, err) {
  motorHat.dcs[0].stopSync();
  motorHat.dcs[1].stopSync();
  console.log("stopped motors");

  if (options.cleanup) {

  }
  if (err) console.log(err.stack);
  if (options.exit) process.exit();
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));

// speed control

function setSpeed(motor, speed) {
  // if ( Math.abs(speed) > 100 )
  //   throw error("Speed cannot be higher than 100!")

  let dir = (MOTOR_WIRING_DIRECTION*speed >= 0) ? 'fwd' : 'back';
  motor.runSync(dir);
  motor.setSpeedSync(Math.abs(speed));
}

exports.setRunLR = function() {
  setSpeed(motorHat.dcs[1], 0 );
  setSpeed(motorHat.dcs[0], 0 ); 
}

exports.setSpeedLR = function(leftSpeed, rightSpeed) {
  setSpeed(motorHat.dcs[1], leftSpeed );
  setSpeed(motorHat.dcs[0], rightSpeed);
};

exports.stopAll = function() {
  motorHat.dcs[0].stopSync();
  motorHat.dcs[1].stopSync();
  clearInterval(exports.h_demo);
}

exports.demo = function() {

  // globals
  const MOTOR_SPEED_MAX  = 50;
  const MOTOR_SPEED_STEP = 10;
  let motor_speed = MOTOR_SPEED_MAX;

  // run main loop
  function mainLoop() {
    if ( motor_speed >= MOTOR_SPEED_MAX ) {
      motor_speed = -MOTOR_SPEED_MAX;
    }

    console.log('motor demo main loop, setting speed:', motor_speed, '...');      
    exports.setSpeedLR(motor_speed, motor_speed);

    motor_speed += MOTOR_SPEED_STEP;
  }
  exports.h_demo = setInterval(mainLoop, 2000);

}
