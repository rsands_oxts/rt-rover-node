var ctxTrack = document.getElementById("myChart").getContext('2d');
var ctxTime  = document.getElementById("timeChart").getContext('2d');

var scatterChart = new Chart(ctxTrack, {
    type: 'line',
    data: {
        datasets: []
    },
    options: {
        scales: {
            xAxes : [{
                type: 'linear',
                position: 'bottom',
                ticks : {
                    max : +0.5    *100,    //cm
                    min : -2  *100
                }
            }],
            yAxes : [{
                type: 'linear',
                position: 'left',
                ticks : {
                    max : 0.5    *100,    
                    min : -2 *100
                }
            }]
        },
        animation: false,
        maintainAspectRatio: true,
        legend: false
        
    },
});

var timeChart = new Chart(ctxTime, {
    type: 'line',
    data: {
        datasets: []
    },
    options: {
        scales: {
            xAxes : [{
                type: 'linear',
                position: 'bottom'
            }],
            yAxes : [
                {
                    id: 'motor_speed',
                    type: 'linear',
                    position: 'left'
                },
                {
                    id: 'small',
                    type: 'linear',
                    position: 'right'
                }
            ]
        },
        animation: false
        
    },
});

$.ajax('/path2d').done(function(data_path2d) {

    let chartdata = data_path2d.concat(data_path2d[0]);

    scatterChart.data.datasets[0] = {
        label: 'Path',
        lineTension: 0,
        data: _.map(chartdata, (p) => {
            return getPoint(p)
        }),
        backgroundColor: 'transparent',
        borderColor: 'red'
    }
    scatterChart.update();
    
});


function poll() {

    $.ajax('/track').done(function(data_track) {
        console.log(data_track);
        // return;

        let positions = _.pluck(data_track, 'Position');

        // position
        scatterChart.data.datasets[1] = {
            label: 'Position',
            lineTension: 0,
            data: _.map(positions, (p) => {
                return getPoint(p)
            }),
            backgroundColor: 'transparent',
            borderColor: 'green'
        }

        scatterChart.data.datasets.splice(2,scatterChart.data.datasets.length);

        // arm line
        let s = scatterChart.data.datasets.length;
        data_track.forEach((d, i) => {
            if (!d.arm_line) return;
            scatterChart.data.datasets[i+s] = {
                label: d.i,
                lineTension: 0,
                data: [
                    // current segment
                    getPoint( d.arm_line[0] ),
                    getPoint( d.arm_line[1] )
                ],
                backgroundColor: 'transparent',
                // borderColor: 'grey'
            }
                
        });

        // arm line ahead
        s = scatterChart.data.datasets.length;
        data_track.forEach((d, i) => {
            if (!d.arm_line_ahead) return;
            scatterChart.data.datasets[i+s] = {
                label: d.i,
                lineTension: 0,
                data: [
                    // current segment
                    getPoint( d.arm_line_ahead[0] ),
                    getPoint( d.arm_line_ahead[1] )
                ],
                backgroundColor: 'transparent',
                borderDash: [5, 15],
                // borderColor: 'grey'
            }
                
        });

        // update
        scatterChart.update();

        // time chart
        timeChart.data.datasets[0] = {
            label: 'lat_dev',
            data: _.map(data_track, (d) => {
                return { x: d.t, y: d.lat_dev }
            }),
            yAxisID: 'small',
            backgroundColor: 'transparent',
            borderColor: 'red'
        };
        timeChart.data.datasets[1] = {
            label: 'left_speed',
            data: _.map(data_track, (d) => {
                return { x: d.t, y: d.left_speed }
            }),
            yAxisID: 'motor_speed',
            backgroundColor: 'transparent',
            borderColor: 'lightblue'
        };
        timeChart.data.datasets[2] = {
            label: 'right_speed',
            data: _.map(data_track, (d) => {
                return { x: d.t, y: d.right_speed }
            }),
            yAxisID: 'motor_speed',
            backgroundColor: 'transparent',
            borderColor: 'lightblue'
        };

        timeChart.update();

    });

}

poll();
setInterval(poll,10000);

function getPoint(ne) {
    return { x: -ne.e*100, y: -ne.n*100 } // cm
}