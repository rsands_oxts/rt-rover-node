const _ = require('underscore');
const dgram = require('dgram');

// const PORT = 3000;
const BIND_HOST = '255.255.255.255';//'255.255.255.255';

module.exports.send = function(address, port, message) {
    const client = dgram.createSocket('udp4');

    let buffer = new Buffer(message);

    client.send(message, 0, message.length, port, address, function(err, bytes) {
        if (err) throw err;
        console.log('UDP message sent to ' + address +':'+ port);
        client.close();
    });    

};

module.exports.listen = function(address, port, cb) {
    const server = dgram.createSocket('udp4');

    server.bind(port, BIND_HOST);

    server.on('listening', function () {
        var address = server.address();
        console.log('UDP Server listening on ' + address.address + ":" + address.port);
    });

    server.on('message', function (message, remote) {
        if ( address != remote.address || port != remote.port ) 
            return

        // decode message
        let buffer = Buffer.from( message, 'utf8' );
        ncom = decodePacket( buffer );

        if (!ncom) 
            return

        cb(ncom, remote);    
    });

};


const NAV_STATUS_VALID = [0,1,2,3,4,5,6,7,10,20,21,22];

function decodePacket(buffer) {

    // checks

    if ( buffer[0] !== 0xE7 ) {
        console.log('Sync byte wrong');
        return
    }
        
    if ( !_.contains(NAV_STATUS_VALID, buffer[21]) ) {
        console.log('Navigation status invalid');
        return
    }

    if ( !checksum(buffer, 61) ) {          // checksum 2
        console.log('Checksum failed');
        return
    }

    let ncom = {};

    // navigation status
    if      ( buffer[21] === 0 ) ncom.status = 0;
    else if ( buffer[21] === 1 ) ncom.status = 1;
    else if ( buffer[21] === 2 ) ncom.status = 2;
    else if ( buffer[21] === 3 ) ncom.status = 3;
    else if ( buffer[21] === 4 ) ncom.status = 4;
    else {
        console.log('Navigation status non standard');
        return
    }

    const emptyBuffer5 = Buffer.alloc(1);

    // position
    ncom.latitude  = buffer.slice(23,31).readDoubleLE() * 180 / Math.PI;
    ncom.longitude = buffer.slice(31,39).readDoubleLE() * 180 / Math.PI;
    

    // heading
    heading_raw = (
        Math.pow(2,0)  * buffer.slice(52).readUIntLE() +
        Math.pow(2,8)  * buffer.slice(53).readUIntLE() +
        Math.pow(2,16) * buffer.slice(54).readUIntLE()
    ) * Math.pow(10, -6) * 180 / Math.PI;
    ncom.heading = ( heading_raw <=180 ) ? heading_raw : heading_raw - 600;

    // console.log(ncom.status, buffer.slice(52,55), ncom.heading);
    return ncom;

}

function checksum(buffer, checksumByte) {
    let csum = 0;
    for (i=1; i<checksumByte; i++) {
        csum += buffer[i];
    }
    return (csum % 256 === buffer[checksumByte]);
}