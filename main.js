"use strict";

const program = require('commander');
const _ = require('underscore');

const csv = require('csvtojson');
const math = require('mathjs');
const intersections = require('kld-intersections');

program
  .version('0.1.0', '-v --version')
  .option('-l, --logging', 'Enable logging')
  .option('-t, --testing', 'Run in test mode (get position from testing.js)')
  .option('--no-motors', 'Disable motor output')
  .option('--no-ncom', 'Disable ncom')
  .option('-F, --freq', 'Frequency of control loop (Hz), default 1Hz')
  .option('-G, --gain', 'Gain for control loop, default 1.0')
  .option('-W, --arm-width', 'Width of lookahead arm (and current segment arm), default 0.100m')
  .option('-L, --arm-look-ahead', 'Lookahead distance, default 0.100m')
  .option('-S, --arm-segment-check-limit', 'Number of segments to check before giving up, default 0.100m')
  .option('--rt-address [address]', '[192.168.25.38]', '192.168.25.38')
  .option('--rt-port    [port]',    '3000', '3000')
  .option('--rt-port-ccom [port-ccom]', '3001', '3001')
  .option('--imu-to-mid-axle-forward [forward]', 'Axle midpoint from IMU (in vehicle frame) in m [ 0.100]',  '0.100')
  .option('--imu-to-mid-axle-right   [right]  ', 'Axle midpoint from IMU (in vehicle frame) in m [-0.150]', '-0.015')
  .parse(process.argv);

const TESTING =  program.testing;

const readline = require('readline');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

const motors = (program.motors) ? require('./lib/motors') : false;
const ncom   = (program.ncom)   ? require('./lib/ncom')   : false;
const web = require('./web/app');

const RT_ADDR = program.rtAddress;
const RT_PORT = program.rtPort;
const RT_PORT_CCOM = program.rtPortCcom;

let ncom_latest = {};
let t_ncom_latest;
if (ncom) 
    ncom.listen(RT_ADDR, RT_PORT, (ncom_decoded, remote) =>  {
        _.extend(ncom_latest, ncom_decoded);
        t_ncom_latest = (new Date()).getTime();
    });

if (!TESTING) {
    setInterval(function() {
        if ( !t_ncom_latest || (new Date()).getTime()-t_ncom_latest > (5000+1000) ) {
            log('(ncom) ---');
            return;
        }
        log('(ncom)', ncom_latest.status);
    }, 5000);
}

if (!motors)
    console.warn("Motors not installed!");


// iterations
const PERIOD = 1 / 20;         // sec, iteration period
// path following
let MAX_SPEED = 40;
let START_SPEED = 10;
let FORWARD_SPEED = START_SPEED;
let LAT_DEV_GAIN_P = 2;
let LAT_DEV_GAIN_D = 2;
let PF_LOOKAHEAD_DISTANCE  = 0.15;        // m
let PF_LOOKAHEAD_DISTANCE_SHORT  = 0.05;        // m
let PF_LOOKAHEAD_ARM_WIDTH = 0.30;        // m
let PF_FIND_SEGMENT_MAX    = 10;          // number of segments to search for intersection before quitting
// (Note that PF_LOOKAHEAD_ARM is used for current segment determination as well.)
let WAIT_AFTER_INIT_S     = 7;            // s to wait after initialising before starting run

// add to get more left wheel
let STEER_CORR = -0.02;

let log;
if (program.logging) {
    log = function(...args) {
        console.log(...args);
    };
} else {
    log = function() {
        // do nothing
    };
};


let controlLoop;        // function
let h_controlLoop;
let t_controlLoop;      // start time
let t_max_corr;

const EVAL_PREFIX = 'eval ';
rl.on('line', function(line){

    // eval

    if ( line.indexOf(EVAL_PREFIX) === 0 ) {
        eval( line.replace(EVAL_PREFIX, '') );
        return;
    }

    // set speed

    if ( line.indexOf('speed') === 0 ) {
        let speedStr = line.replace("speed", '')
        MAX_SPEED = parseFloat(speedStr);
        console.log('Set speed to', MAX_SPEED);
        return;
    }

    if ( line.indexOf('steer') === 0 ) {
        let speedStr = line.replace("steer", '')
        let steerCorr = parseFloat(speedStr);
        STEER_CORR = steerCorr;
        console.log('STEER_CORR set to', steerCorr);
        return;
    }

    if ( line.indexOf('calspeed') === 0 ) {
        let speedStr = line.replace("calspeed", '')
        let calSpeed = parseFloat(speedStr);
        console.log('Speed calibration: both wheels set to', calSpeed);
        if (motors) motors.setRunLR();
        setTimeout(function() {
            if (motors) motors.setSpeedLR(
                calSpeed*( 1 + STEER_CORR ), calSpeed*( 1 - STEER_CORR )
            );
        }, 1000);
        setTimeout(function() {
            if (motors) motors.stopAll();
        }, 4000);

        return;
    }

    // simple commands

    switch(line) {
        case '':
            break;
        case 'r':
            console.log("resetting RT...");
            resetRt();
            break;
        case 'i':
            console.log("sending commands to initialise RT...");            
            initRt();
            break;
        case 'x':
            console.log("stopping...");
            stopControlLoop();
            break;
        case 's':
            console.log("starting control loop when ncom ready...")
            checkToStart();
            break;
        default:
            console.log("Invalid command")
    }

});

function deg2rad(deg) {
    return deg*Math.PI/180;
}

// rover geometry
console.dir('imuToMidAxleRight', program.imuToMidAxleRight)
const IMU_TO_MID_AXLE = {
    n: parseFloat(program.imuToMidAxleForward),
    e: parseFloat(program.imuToMidAxleRight  )
};
console.dir(IMU_TO_MID_AXLE)
const ROVER_WIDTH_HALF   = 0.055;   // m from pivot
const ROVER_LENGTH_LASER = 0.035;
const ROVER_LENGTH_REAR  = 0.130;
const AXLE_LENGTH = 0.1;            // m

// lat/lng conversion (simple constants)
const M_PER_LAT =  111.32 * 1000;
const M_PER_LNG =  111.32 * 1000 * Math.cos(deg2rad(52));

// errors
const ERR_NCOM_POS = ((0.020^2)/2)^0.5      // m
const ERR_NCOM_HEA = deg2rad(0.010)         // rad

// arrays
// let Track;   // TODO:Track
let Position = 0;
let Heading = 0;
let CurrentSegIdx = 0;
let i_controlLoop = 0;

let Track = {
    size: 500,
    current: -1,
    data: [],
    add: (elem) => {
        // increment current
        let index = ++Track.current;
        // delete old object
        delete Track.data[index];
        // save new element
        Track.data[index] = elem;
        // cycle current
        if ( Track.current === Track.size-1 )
            Track.current = -1;

        return elem;
    },
    getCurrent: () => {
        let index = (Track.current > -1) ? Track.current : Track.size-1;
        return Track.data[index];
    },
    getAll: () => {
        let index = (Track.current+1);
        let result = [];
        let elem;
        for(let j=0;j<Track.size;j++){
            elem = Track.data[(index+j)%Track.size]
            if (elem) result.push( elem );
        }
        return result;
    },
    reset: () => {
        Track.current = 0;
        Track.data = [];
    }
};

let Path2d;         // arrays will be initialised later
let Segments2d;

function resetRt() {
    function send() {
        ncom.send(RT_ADDR, RT_PORT_CCOM, '!reset \r\n');
    }
    send();
    setTimeout(send, 1000);
    setTimeout(send, 2000);
    setTimeout(send, 3000);
}

function initRt(cb) {
    console.log("Checking for ncom...")

    let h_check_status = setInterval(function() {
        // check for ncom status 1
        if (ncom_latest.status >= 1) {
            clearInterval(h_check_status);

            console.log("Sending time, then waiting...");

            let now = new Date();
            var start1980 = new Date(1980, 0, 0);
            var diff = now - start1980;
            var gpsWeek = diff / (7 * 24 * 60 * 60 * 1000);
            var gpsTimeIntoWeek = ( diff / 1000 ) % 604800
        
            ncom.send(RT_ADDR, RT_PORT_CCOM, `!set time target ${gpsWeek} ${gpsTimeIntoWeek} \r\n`);
            setTimeout(function() {
                console.log("Sending position, velocity, heading...");
                let strPos = '!set init aidpos 51.9414142934745 -1.2489584582655 130 0.02 \r\n';
                let strVel = '!set init aidvel 0 0 0 0.001 \r\n';
                let strHea = '!set init hea 64.296518179424 \r\n';
        
                ncom.send(RT_ADDR, RT_PORT_CCOM, strPos+strVel+strHea);
                console.log("Waiting for initialisation...");
                if (cb) cb();
            }, 15000);

        }
    }, 500);



}

function checkToStart() {
    if (TESTING) {
        startControlLoop();
        return;
    }

    if (motors) motors.setRunLR();
    let h_check_status = setInterval(function() {
        // check for ncom status 4
        if (ncom_latest.status == 4) {
            console.log(`Initialised, waiting ${WAIT_AFTER_INIT_S} seconds`)
            setTimeout(startControlLoop, WAIT_AFTER_INIT_S*1000);
            clearInterval(h_check_status);
        }
    }, 500);
}

function startControlLoop() {
    Track.reset();

    // initialise global values
    Position = { n: 0, e: 0 };
    Heading  = 0;    
    CurrentSegIdx = Path2d.length-1;
    i_controlLoop = 0;

    let FORWARD_SPEED = START_SPEED;

    // if (motors) motors.setRunLR();
    t_controlLoop = (new Date()).getTime();
    h_controlLoop = setInterval(controlLoop, PERIOD*1000);
}

function stopControlLoop() {
    clearInterval(h_controlLoop);
    if (motors) motors.stopAll();
    resetRt();
}

csv({
    noheader:true,
	colParser:{
		"field1":"number",
		"field2":"number",
		"field3":"omit",
		"field4":"number",
		"field5":"number",
		"field6":"omit",
		"field7":"omit",
		"field7":"omit"
	}    
})
.fromFile('./mobile.seg')
.then((segments_LAT_LNG)=>{

    let s = segments_LAT_LNG[0];
    let origin_LAT_LNG = midPoint({n: s.field1, e: s.field2}, {n: s.field4, e: s.field5});

    Segments2d = [];
    segments_LAT_LNG.forEach((s) => {
        Segments2d.push([
            (s.field1 - origin_LAT_LNG.n) * M_PER_LAT,
            (s.field2 - origin_LAT_LNG.e) * M_PER_LNG,
            (s.field4 - origin_LAT_LNG.n) * M_PER_LAT,
            (s.field5 - origin_LAT_LNG.e) * M_PER_LNG
        ]);
    });
    Path2d = calc_path(Segments2d);
    web.getData().Path2d = Path2d;

    s = Segments2d[0];

    // create starting point for track
    // const track_points_in_rover_frame = [ 
    //     { n: 0,  e: -AXLE_LENGTH/2 },
    //     { n: 0,  e: 0              },
    //     { n: 0,  e:  AXLE_LENGTH/2 }
    // ]
    // let start_track_points = transform_points_NE_H(track_points_in_rover_frame, Position, Heading);
        
    log('Position', Position);
    log('Heading', Heading);
    // log('start_track_points', start_track_points);

    log('Segments2d', Segments2d.length, Segments2d.slice(0,5));
    log('Path2d', Path2d.length,     Path2d.slice(0,5));

    // runpathfollow

    // variables calculated during loop
    let arm_line, arm_line_in_rover, arm_line_ahead, arm_line_ahead_in_rover;
    let intersec, intersec_path_line;
    let intersec_ahead, intersec_path_line_ahead, ahead_seg_idx;
    let lat_dev = 0;
    let inside_speed = 0, left_speed = 0, right_speed = 0;
    let max_corr = 0;

    controlLoop = function() {
        let i = i_controlLoop;
        let t = (new Date()).getTime()-t_controlLoop;
        log('***', i, '***', t);

        if (TESTING) {
            const testing = require('./lib/testing.js');
            if (i >= testing.Position.length) {
                console.log("End of test data reached.");
                stopControlLoop();
                return;
            }
            Position.n = testing.Position[i][0];
            Position.e = testing.Position[i][1];
            Heading  = testing.Heading[i];
        } else {
            log(ncom_latest)
            Position.n = (ncom_latest.latitude  - origin_LAT_LNG.n) * M_PER_LAT;
            Position.e = (ncom_latest.longitude - origin_LAT_LNG.e) * M_PER_LNG;
            Heading    = (ncom_latest.heading)*Math.PI/180;
        }

        log('origin_LAT_LNG', origin_LAT_LNG);
        log('Position, Heading', Position, Heading);

        // add to tracks
        let track = Track.add({ 
            i: i, 
            t: t,
            Position: _.clone(Position), 
            Heading: Heading
        });
        web.getData().Track = Track.getAll();
        
        // calculate path intersections - ahead segment

        [ arm_line_ahead, arm_line_ahead_in_rover ] = get_arm_for_pos_and_hea(Position, Heading, PF_LOOKAHEAD_DISTANCE);
        [ ahead_seg_idx, intersec_ahead, intersec_path_line_ahead ] = find_intersecting_segment_idx(Path2d, CurrentSegIdx, arm_line_ahead);
        track.ahead_seg_idx = ahead_seg_idx;
        track.arm_line_ahead = arm_line_ahead;
        log('> ahead_seg_idx', ahead_seg_idx);

        if (ahead_seg_idx === -1) {
            // no segment found
            console.warn ("AHEAD SEGMENT NOT FOUND");
            // fallback to short

            [ arm_line_ahead, arm_line_ahead_in_rover ] = get_arm_for_pos_and_hea(Position, Heading, PF_LOOKAHEAD_DISTANCE_SHORT);
            [ ahead_seg_idx, intersec_ahead, intersec_path_line_ahead ] = find_intersecting_segment_idx(Path2d, CurrentSegIdx, arm_line_ahead);
            track.ahead_seg_idx = ahead_seg_idx;
            track.arm_line_ahead = arm_line_ahead;
            log('> ahead_seg_idx', ahead_seg_idx);
            // TODO: log we are using short
            
            if (ahead_seg_idx === -1) {
                // short ahead segment not found
                // TODO: something clever?
            }

        }

        // calculate path intersections - current segment

        [ arm_line, arm_line_in_rover ] = get_arm_for_pos_and_hea(Position, Heading, 0);
        [ CurrentSegIdx, intersec, intersec_path_line ] = find_intersecting_segment_idx(Path2d, CurrentSegIdx, arm_line);
        track.current_seg_idx = CurrentSegIdx;
        track.arm_line = arm_line;
        log('> CurrentSegIdx', CurrentSegIdx);

        if (CurrentSegIdx === -1) {
            // no segment found
            console.warn ("CURRENT SEGMENT NOT FOUND");
            stopControlLoop();
            return;
        }

        // calculate heading deviation

        if ( ahead_seg_idx == -1) {
            // use current (short range)
            log('using current');
            lat_dev = calc_lateral_deviation( intersec, intersec_path_line, arm_line_in_rover );
        } else {
            lat_dev = calc_lateral_deviation( intersec_ahead, intersec_path_line_ahead, arm_line_ahead_in_rover );
        }
        track.lat_dev = lat_dev;

        log('lat_dev', lat_dev);

        // calculate required wheel speeds (lat_dev +ve to right when looking forwards from rover )

        max_corr = Math.min( 0.4, 0.0001*( (new Date()).getTime() - t_controlLoop ) );
        log('max_corr', max_corr);

        FORWARD_SPEED = Math.round( Math.min( MAX_SPEED, START_SPEED + 0.01*((new Date()).getTime()-t_controlLoop) ) );

        let speed_corr = Math.min( Math.abs(lat_dev)*LAT_DEV_GAIN, max_corr );
        track.speed_corr = speed_corr;

        if (lat_dev < 0) {
            // need to turn left
            left_speed =  FORWARD_SPEED * ( 1 + speed_corr ) * ( 1 + STEER_CORR );
            right_speed = FORWARD_SPEED * ( 1 - speed_corr ) * ( 1 - STEER_CORR );
        } else {
            // need to turn right
            left_speed =  FORWARD_SPEED * ( 1 - speed_corr ) * ( 1 + STEER_CORR );
            right_speed = FORWARD_SPEED * ( 1 + speed_corr ) * ( 1 - STEER_CORR );
        }
        track.left_speed  = left_speed;
        track.right_speed = right_speed;
        
        // set wheel speeds
        log('motors set speed', left_speed, right_speed)
        track.left_speed = left_speed;
        track.right_speed = right_speed;

        if (motors && !TESTING) motors.setSpeedLR(left_speed, right_speed);

        i_controlLoop++;
    }

});

function get_next_segment_idx(path2d, idx) {
    // get next index with wrap around

    let next_idx = idx+1;
    if ( next_idx > path2d.length-1 )
        next_idx = next_idx-path2d.length;

    return next_idx;
}

function find_intersecting_segment_idx(path2d, segment_idx, arm_line_points) {
    // TODO: optimise variables

    let attempts = 0;
    let intersec_obj, path_line_points;
    log("> arm_line_points \n", arm_line_points[0], arm_line_points[1]);
    while ( 1 ) {

        let next_segment_idx = get_next_segment_idx(path2d, segment_idx);
        path_line_points = [ path2d[segment_idx], path2d[next_segment_idx] ]
        //path2d.slice(segment_idx, next_segment_idx+1);
        log("  > segment_idx next_segment_idx", segment_idx, next_segment_idx);
        log("  > path_line_points\n", path_line_points[0], path_line_points[1]);

        // find intersection of path_line and arm_line
        intersec_obj = intersections.Intersection.intersectLineLine(
            // path_line
            new intersections.Point2D(path_line_points[0].n, path_line_points[0].e),
            new intersections.Point2D(path_line_points[1].n, path_line_points[1].e),
            // arm_line
            new intersections.Point2D(arm_line_points[0].n , arm_line_points[0].e ),
            new intersections.Point2D(arm_line_points[1].n , arm_line_points[1].e )
        );

        //log(intersec_obj);

        if ( intersec_obj.status == "Intersection" )
            // intersection found
            break

        // try next segment
        segment_idx = next_segment_idx;

        attempts += 1;
        if ( attempts >= PF_FIND_SEGMENT_MAX ) {
            console.warn ("attempts exceeded PF_FIND_SEGMENT_MAX!");
            return [ -1, -1, -1 ];
        }
    }

    // TODO: select correct intersection if there are more than one point
    if ( intersec_obj.points.length !== 1 ) {
        console.warn(intersec_obj.points.length, "intersection points found! (Should be 1)");
    }

    let intersec = {
        n: intersec_obj.points[0].x,
        e: intersec_obj.points[0].y
    };

    // return intersection segment_index, intersection point, and intersection path_line (2 points)
    return [ segment_idx, intersec, path_line_points ];
}

function calc_lateral_deviation(intersec, path_line, arm_line_in_rover) {

    // rover frame
    let path_line_in_rover = transform_points_NE_H(path_line, Position, Heading, true);  // inverse
    let intersec_in_rover  = transform_points_NE_H(intersec,  Position, Heading, true);  // inverse
    
    // lateral deviation
    let lat_dev = _subtractPoints( midPoint(arm_line_in_rover[0], arm_line_in_rover[1]),  intersec_in_rover).e;     // east component of arm midpoint - intersec

    /* NOT NEEDED
    // heading deviation
    let hea_dev = asin(lat_dev);
    */

    return lat_dev;
}

function get_arm_for_pos_and_hea(pos, hea, y=0) {

    log("> pos", pos);
    log("> hea", hea);

    let arm_in_rover = [
        { n: y + IMU_TO_MID_AXLE.n,  e: -PF_LOOKAHEAD_ARM_WIDTH/2 /*+ IMU_TO_MID_AXLE.e*/ },       
        { n: y + IMU_TO_MID_AXLE.n,  e:  PF_LOOKAHEAD_ARM_WIDTH/2 /*+ IMU_TO_MID_AXLE.e*/ }
    ];
    let arm = transform_points_NE_H(arm_in_rover, pos, hea);

    return [arm, arm_in_rover];
}
function transform_points_NE_H(points, NE, H, do_inverse=false) {
    if (!Array.isArray(points)) points = [points];

    let transformedPoints = [];
    points.forEach((p) => {
        let tp;
        
        let homog_mat = math.matrix([
            [ Math.cos(H),  -Math.sin(H),   NE.n ],
            [ Math.sin(H),   Math.cos(H),   NE.e ],
            [ 0,             0,             1    ]
        ]);

        if (do_inverse) {
            homog_mat = math.inv(homog_mat);
        }

        let p_mat = math.matrix([ [p.n], [p.e], [1] ]);
        let tp_mat = math.multiply(homog_mat, p_mat);
        let tp_arr = tp_mat.toArray();

        transformedPoints.push({
            n: tp_arr[0][0],
            e: tp_arr[1][0]
        });

    });

    if (transformedPoints.length == 1) 
        return transformedPoints[0];

    return transformedPoints;
}

function _subtractPoints(p1,p2) {
    return {
        n: p1.n - p2.n,
        e: p1.e - p2.e
    }
}

function get_heading_from_wheels(left_wheel_NE, right_wheel_NE) {
    let dN_dE = _subtractPoints(right_wheel_NE, left_wheel_NE);
    return Math.atan2(-dN_dE.n, dN_dE.e);
}

function calc_path(segments) {
    let path_segments = [];
    segments.forEach((s) => {
        // add midpoint of this segment
        let p1 = midPoint({n: s[0], e: s[1]}, {n: s[2], e: s[3]});
        path_segments.push(p1);
    });
    return path_segments;
}

function midPoint(p1, p2) {
    return {
        n: p1.n + ( p2.n - p1.n )/2,
        e: p1.e + ( p2.e - p1.e )/2
    }
}
