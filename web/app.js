const express = require('express')
const app = express()

app.set('view engine', 'pug')
app.set('views', './web/views')
app.use(express.static('./web/bower_components'))
app.use(express.static('./web/public'))

app.get('/', function (req, res) {
    res.render('index', { title: 'NCOM web' })
});
  
app.listen(8080, () => console.log('Example app listening on port 8080!'))

let data = {
    Path2d: [],
    Track: []
};
module.exports.getData = function() {
    return data;
};

app.get('/path2d', function(req, res) {
    res.json(data.Path2d);
});

app.get('/track', function(req, res) {
    res.json(data.Track);
});